package models

import (
	"github.com/revel/revel"
)

type Timelog struct {
	ID       string
	Title    string
	Details  string
	Duration int
	Color    int
	Deleted  int
	AddUnix  int64
	EditUnix int64
}

func (item *Timelog) Validate(v *revel.Validation, validateType string) {

	v.Required(item.Title).MessageKey("Title Required")
	v.MaxSize(item.Title, 100).MessageKey("Title Should Be Less Than 100 Characters")

	v.Required(item.Duration).MessageKey("Duration Required")
	v.Range(item.Duration, 1, 9999999999).MessageKey("Duration Should Be Between 1 and 999999999")

	v.MaxSize(item.Details, 500).MessageKey("Details Should Be Less Than 500 Characters")
	v.Range(item.Color, 1, 4).MessageKey("Color Should Be Between 1 and 4")

	if validateType == "create" {

	}

}
