package controllers

import "strings"

//APIRespone ...
type APIRespone struct {
	Code     string
	Message  string
	Messages interface{}
	Data     interface{}
	Page     interface{}
	NextPage interface{}
	Size     interface{}
}

type QueryError struct {
	Code    string
	Message string
}

func GetQueryErrorDetails(querErr error) QueryError {
	QueryDetails := QueryError{}

	if strings.Contains(querErr.Error(), "UNIQUE") {
		QueryDetails.Message = "Duplicate Record"
		QueryDetails.Code = "Duplicate"
	} else {
		QueryDetails.Message = "Query Unknown Error"
		QueryDetails.Code = "QueryError"
	}

	return QueryDetails
}
