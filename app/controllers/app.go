package controllers

import (
	"TimeTracking/app/models"
	"fmt"
	"strings"
	"time"

	"github.com/revel/modules/orm/gorp/app/controllers"
	"github.com/revel/revel"
	uuid "github.com/satori/go.uuid"
	squirrel "gopkg.in/Masterminds/squirrel.v1"

	_ "github.com/google/uuid"
)

type Application struct {
	gorpController.Controller
}

func (c Application) Option() revel.Result {
	respone := APIRespone{}
	return c.RenderJSON(respone)
}

func (c Application) List(search string, page uint64, size uint64, delete int) revel.Result {
	respone := APIRespone{}

	if page <= 0 {
		page = 1
	}
	if size <= 0 {
		size = 10
	} else if size > 100 {
		size = 100
	}

	nextPage := page + 1
	search = strings.TrimSpace(search)

	var items []*models.Timelog
	builder := c.Db.SqlStatementBuilder.Select("*").From("timelog").Offset((page - 1) * size).Limit(size).OrderBy("AddUnix DESC")

	if delete == 1 {
		builder = builder.Where("deleted = 1 OR deleted = 0")
	} else if delete == 2 {
		builder = builder.Where("deleted = 1")
	} else {
		builder = builder.Where("deleted = 0")
	}
	if search != "" {

		builder = builder.Where(squirrel.Or{
			squirrel.Expr("lower(Details) like ?", "%"+strings.ToLower(search)+"%"),
			squirrel.Expr("Duration = ?", strings.ToLower(search))})
	}
	if _, err := c.Txn.Select(&items, builder); err != nil {
		c.Log.Error("List", "error", err)
		respone.Code = "NotFound"
		respone.Message = "Select Query Cannot Find Any Data"
		return c.RenderJSON(respone)
	}

	respone.Data = items
	respone.Size = size
	respone.Page = page
	respone.NextPage = nextPage

	return c.RenderJSON(respone)

}

//Get ...
func (c Application) Get(id string) revel.Result {
	respone := APIRespone{}
	item := c.getByID(id)
	if item == nil {
		respone.Code = "NotFound"
		respone.Message = fmt.Sprintf("Record ID %s Not Found", id)
		return c.RenderJSON(respone)

	}

	respone.Data = []interface{}{item}
	return c.RenderJSON(respone)
}

//Save ...
func (c Application) Save(item models.Timelog) revel.Result {

	respone := APIRespone{}
	item.Validate(c.Validation, "create")

	if c.Validation.HasErrors() {
		respone.Code = "Validation"
		respone.Message = c.Validation.Errors[0].String()
		respone.Messages = c.Validation.Errors
		return c.RenderJSON(respone)
	}
	unix := time.Now().Unix()
	item.Deleted = 0

	item.AddUnix = unix
	item.EditUnix = unix
	ItemUUID, err := uuid.NewV4()

	if err != nil {
		respone.Code = "UUID"
		respone.Message = "Cannot Generate UUID"
		return c.RenderJSON(respone)
	}
	item.ID = fmt.Sprintf("%v", ItemUUID)
	err = c.Txn.Insert(&item)
	if err != nil {
		QueryDetails := GetQueryErrorDetails(err)

		respone.Message = QueryDetails.Message
		respone.Code = QueryDetails.Code
		return c.RenderJSON(respone)

	}

	return c.RenderJSON(respone)
}

//Update ...
func (c Application) Update(id string, item models.Timelog) revel.Result {
	respone := APIRespone{}
	oldItem := c.getByID(id)

	if oldItem == nil {
		respone.Code = "NotFound"
		respone.Message = fmt.Sprintf("Record ID %v Not Found", id)
		return c.RenderJSON(respone)

	}

	item.Validate(c.Validation, "update")
	if c.Validation.HasErrors() {
		respone.Code = "Validation"
		respone.Message = c.Validation.Errors[0].String()
		respone.Messages = c.Validation.Errors
		return c.RenderJSON(respone)
	}
	if len(item.Details) != 0 {
		oldItem.Details = item.Details
	}

	if item.Duration != 0 {
		oldItem.Duration = item.Duration
	}

	_, err := c.Txn.Update(&oldItem)
	if err != nil {
		respone.Code = "Query"
		respone.Message = "Update Error"
		return c.RenderJSON(respone)

	}

	return c.RenderJSON(respone)
}

func (c Application) Archive(id string) revel.Result {

	respone := APIRespone{}

	item := c.getByID(id)
	if item == nil {
		respone.Code = "NotFound"
		respone.Message = fmt.Sprintf("Record ID %s Not Found", id)
		return c.RenderJSON(respone)
	}

	item.Deleted = 1

	_, err := c.Txn.Update(item)
	if err != nil {
		respone.Code = "Query"
		respone.Message = "Query Error"
		return c.RenderJSON(respone)
	}

	return c.RenderJSON(respone)

}

func (c Application) UnArchive(id string) revel.Result {

	respone := APIRespone{}
	item := c.getByID(id)
	if item == nil {
		respone.Code = "NotFound"
		respone.Message = fmt.Sprintf("Record ID %s Not Found", id)
		return c.RenderJSON(respone)
	}
	item.Deleted = 0

	_, err := c.Txn.Update(item)
	if err != nil {
		respone.Code = "Query"
		respone.Message = "Query Error"
		return c.RenderJSON(respone)
	}

	return c.RenderJSON(respone)
}

func (c Application) getByID(id string) *models.Timelog {
	h, err := c.Txn.Get(models.Timelog{}, id)
	if err != nil {
		panic(err)
	}
	if h == nil {
		return nil
	}
	return h.(*models.Timelog)
}
