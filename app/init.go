package app

import (
	"TimeTracking/app/models"
	"fmt"
	"strings"

	rgorp "github.com/revel/modules/orm/gorp/app"

	"github.com/revel/revel"
	gorp "gopkg.in/gorp.v2"
)

var (
	// AppVersion revel app version (ldflags)
	AppVersion string

	// BuildTime revel app build-time (ldflags)
	BuildTime string
)

func init() {
	// Filters is the default set of global filters.
	revel.Filters = []revel.Filter{
		revel.PanicFilter,             // Recover from panics and display an error page instead.
		revel.RouterFilter,            // Use the routing table to select the right Action
		revel.FilterConfiguringFilter, // A hook for adding or removing per-Action filters.
		revel.ParamsFilter,            // Parse parameters into Controller.Params.
		revel.SessionFilter,           // Restore and write the session cookie.
		revel.FlashFilter,             // Restore and write the flash cookie.
		revel.ValidationFilter,        // Restore kept validation errors and save new ones from cookie.
		revel.I18nFilter,              // Resolve the requested language
		HeaderFilter,                  // Add some security based headers
		revel.InterceptorFilter,       // Run interceptors around the action.
		revel.CompressFilter,          // Compress the result.
		revel.ActionInvoker,           // Invoke the action.
	}

	revel.OnAppStart(func() {
		Dbm := rgorp.Db.Map

		setColumnSizes := func(t *gorp.TableMap, colSizes map[string]int) {
			for col, size := range colSizes {
				t.ColMap(col).MaxSize = size
			}
		}

		//Create timelog Table

		t := Dbm.AddTableWithName(models.Timelog{}, "timelog").SetKeys(false, "ID")

		setColumnSizes(t, map[string]int{
			"Details":  100,
			"Duration": 12,
		})

		rgorp.Db.TraceOn(revel.AppLog)
		err := Dbm.CreateTables()
		if err == nil {
			err = Dbm.CreateIndex()
			if err != nil {
				panic("Create Index Table error")
			}
		}

		demoItems := []*models.Timelog{
			&models.Timelog{"c1a9a7d7-ad2f-4eba-b867-cc83e850370d", "Demo Title", "I Finished this Demo Task", 992, 1, 0, 1528844787, 1528844787},
		}

		for _, singleItem := range demoItems {
			if err := Dbm.Insert(singleItem); err != nil {
				if strings.Contains(err.Error(), "UNIQUE") {
					fmt.Println("Duplicate")
				}
			}
		}

	}, 5)

	// Register startup functions with OnAppStart
	// revel.DevMode and revel.RunMode only work inside of OnAppStart. See Example Startup Script
	// ( order dependent )
	// revel.OnAppStart(ExampleStartupScript)
	// revel.OnAppStart(InitDB)
	// revel.OnAppStart(FillCache)
}

// HeaderFilter adds common security headers
// There is a full implementation of a CSRF filter in
// https://github.com/revel/modules/tree/master/csrf
var HeaderFilter = func(c *revel.Controller, fc []revel.Filter) {

	c.Response.Out.Header().Add("X-Frame-Options", "SAMEORIGIN")
	c.Response.Out.Header().Add("X-XSS-Protection", "1; mode=block")
	c.Response.Out.Header().Add("X-Content-Type-Options", "nosniff")
	c.Response.Out.Header().Add("Referrer-Policy", "strict-origin-when-cross-origin")
	c.Response.Out.Header().Add("Access-Control-Allow-Origin", "*")
	c.Response.Out.Header().Add("Access-Control-Allow-Headers", "Content-Type")
	c.Response.Out.Header().Add("Access-Control-Allow-Methods", "POST, GET, OPTION, DELETE")

	fc[0](c, fc[1:]) // Execute the next filter stage.
}

//func ExampleStartupScript() {
//	// revel.DevMod and revel.RunMode work here
//	// Use this script to check for dev mode and set dev/prod startup scripts here!
//	if revel.DevMode == true {
//		// Dev mode
//	}
//}
