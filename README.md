# Time Tracking - Back end

This is the Backend for the time tracking project.
backend built with "Go" language.

there is two way to run the project.

- Easy & No-time: if you trust .exe file, just extact "TimeTracking.zip" and than run the "start.bat"
- Harder: 
    * Install golang https://golang.org/
    * Install Revel https://revel.github.io/tutorial/gettingstarted.html
    * Clone this Project under %GOPATH%/TimeTracking , it's important to keep the same name 
    * run "revel run" it should start download all used package, than run the project 
    , if the project failed with error message (missing something, import something etc) please just run
    go get $something ($something = name of missing thing)


### Pros

* Well, it's go (High Performance)
* i used sqllite to make it easier to run and test things 
* Create, Update, Delete, Get All, Get Single, with Validations


### Why I haven't use php Symfony for this project

i have better php experance than golang,
however i haven't the enough time 
to look inside symfony documentation. 
simply because my current job took me 8h:30m every day.
after that i work for 2 hour for my own team office.
so my cute brain can't handle more amount of works :)
and that's why i used something i know about to avoid learning time.

